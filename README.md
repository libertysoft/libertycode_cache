LibertyCode_Cache
=================



Description
-----------

Library contains cache components, 
allows to manage key-value pairs
and to design engine to storage it,
using register.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/cache ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/cache": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### Format data

Simple array data allows to manage formatting item callable.

#### Repository

Repository allows to design specific cache system, 
to manage and prepare items, 
to search, get, set and remove it,
on specific storage support,
from register and specific configuration.

_Elements_

- Repository
    
    Allows to design a repository,
    who is specific cache system containing all information, 
    to manage and prepare item, 
    to set in register.

- FormatRepository

    Extends repository features.
    Allows to configure item formatting publicly, 
    using format data feature,
    to prepare items, to get and set from register.

    Formatting item callable notes:
    - Callable can be set to format item during getting/setting item. 
    - Callable can be set per type/class REGEXP path and/or item key REGEXP. 

_Example_

```php
// Get register
use liberty_code\register\register\memory\model\MemoryRegister;
$register = new MemoryRegister();
...
// Get repository
use liberty_code\cache\repository\model\DefaultRepository;
$repository = new DefaultRepository(
    null,
    $register
);
...
// Set items in repository
$repository->setItem('key_1', '...'); // Register specified item for key 1
$repository->setItem('key_N', '...'); // Register specified item for key N
...
foreach($repository->getTabSearchKey() as $key) {
    var_dump($repository->getItem($key));
}
/**
 * Show: 
 * item for key 1
 * item for key N
 */
...
```

---


