<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\cache\format\exception;

use Exception;

use liberty_code\cache\format\library\ConstFormatData;



class ValueInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $value
     */
	public function __construct($value)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstFormatData::EXCEPT_MSG_VALUE_INVALID_FORMAT, strval($value));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified value has valid format.
	 * 
     * @param mixed $value
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($value)
    {
		// Init var
		$result = is_callable($value); // Check is valid callable
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($value);
		}
		
		// Return result
		return $result;
    }
	
	
	
}