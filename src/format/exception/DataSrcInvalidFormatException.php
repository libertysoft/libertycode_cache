<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\cache\format\exception;

use Exception;

use liberty_code\cache\format\library\ConstFormatData;



class DataSrcInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $dataSrc
     */
	public function __construct($dataSrc)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
		(
            ConstFormatData::EXCEPT_MSG_DATA_SRC_INVALID_FORMAT,
			mb_strimwidth(strval($dataSrc), 0, 10, "...")
		);
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified data source has valid format.
	 * 
     * @param mixed $dataSrc
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($dataSrc)
    {
		// Init var
		$result = is_array($dataSrc);
		
		// Run all data if required
		if($result)
		{
			$tabKey = array_keys($dataSrc);
			for($intCpt = 0; $result && ($intCpt < count($tabKey)); $intCpt++)
			{
				// Get data
				$strKey = $tabKey[$intCpt];
				$strValue = $dataSrc[$strKey];
				
				// Check data
				$result = 
					is_string($strKey) && (trim($strKey) !== '') && // Check key is valid string
					is_callable($strValue); // Check value is valid callable
			}
		}
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($dataSrc) ? serialize($dataSrc) : $dataSrc));
		}
		
		// Return result
		return $result;
    }
	
	
	
}