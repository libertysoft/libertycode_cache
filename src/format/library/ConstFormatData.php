<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\cache\format\library;



class ConstFormatData
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

	// Exception message constants
    const EXCEPT_MSG_DATA_SRC_INVALID_FORMAT =
        'Following data source "%1$s" invalid! 
        The data source must be a valid array, with key as string, not empty and value as callable.';
	const EXCEPT_MSG_VALUE_INVALID_FORMAT = 'Following value "%1$s" invalid! The value must be a callable.';
}