<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\cache\format\model\FormatData;



// Init var
$tabDataSrc = array(
	'config.Load.Get' => function ($strKey, $value) {
	    return 'format_load_get-"' . $value . '"';
    },
    'config.Load.Set' => function ($strKey, $value) {
        return 'format_load_set-"' . $value . '"';
    }
);

$objData = new FormatData();



// Test properties
echo('Test properties : <br />');

try{
	$objData->setDataSrc(array(
		'config.Load.Get' => 'Test 1',
		'config.Load.Set' => 1
	));
} catch(\Exception $e) {
	echo($e->getMessage());
	echo('<br />');
}

$objData->setDataSrc($tabDataSrc);

echo('Source data: <pre>');print_r($objData->getDataSrc());echo('</pre>');

echo('<br /><br /><br />');



// Test put
$tabKey = array(
    // Found
    'config.Di.Get' => function ($strKey, $value) {
        return unserialize($value);
    },

    // Found
    'config.Di.Set' => function ($strKey, $value) {
        return serialize($value);
    },

    'config/Di/Set' => 'Value test', // Bad value format test
    'test' => 7.7 // Bad value format test
);

foreach($tabKey as $strKey => $value)
{
	echo('Test put "'.$strKey.'": <br />');
	try{
		echo('<pre>');var_dump($objData->putValue($strKey, $value));echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test putting: <br />');
echo('<pre>');print_r($objData->getDataSrc());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');



// Test check, get
$tabKey = array(
    'config.Load.Get' => 'Value test', // Found
    'config.Load.Set' => 7, // Found
    'config/Load/Set' => 'Value test', // Bad path format test
    'test' => 7.5, // Not found

    // Found
    'config.Di.Get' => serialize(array(
        'key_test_get_1' => 'Value test 1',
        'key_test_get_2' => 'Value test 2',
        'key_test_get_N' => 'Value test N',
    )),

    // Found
    'config.Di.Set' => array(
        'key_test_set_1' => 'Value test 1',
        'key_test_set_2' => 'Value test 2',
        'key_test_set_N' => 'Value test N',
    )
);

foreach($tabKey as $strKey => $value)
{
    echo('Test check, get "'.$strKey.'": <br />');
    try{
        $callable = $objData->getValue($strKey);

        if($objData->checkValueExists($strKey))
        {
            echo('<pre>');var_dump($callable($strKey, $value));echo('</pre>');
        }
        else
        {
            echo('Key not found<br />');
        }
    } catch(\Exception $e) {
        echo(htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('<br /><br /><br />');



// Test remove
$tabKey = array(
    'config.Load.Get', // Found
    // 'config.Load.Set', // Found
	'test', // Not found
    // 'config.Di.Get', // Found
    'config.Di.Set', // Found
    'config/Di/Get' // Not found
);

foreach($tabKey as $strKey)
{
	echo('Test remove "'.$strKey.'": <br />');
	try{
		echo('<pre>');var_dump($objData->removeValue($strKey));echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test removing: <br />');
echo('<pre>');print_r($objData->getDataSrc());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');


