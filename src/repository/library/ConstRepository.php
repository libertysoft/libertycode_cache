<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\cache\repository\library;



class ConstRepository
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_REGISTER = 'objRegister';
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';



    // Configuration
    const TAB_CONFIG_KEY_KEY_PATTERN = 'key_pattern';
    const TAB_CONFIG_KEY_KEY_REGEXP_SELECT = 'key_regexp_select';



    // Exception message constants
    const EXCEPT_MSG_REGISTER_INVALID_FORMAT = 'Following register "%1$s" invalid! It must be a register object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! The config must be an array, not empty and following the default configuration standard.';
    const EXCEPT_MSG_KEY_INVALID_FORMAT = 'Following key "%1$s" invalid! The key must be a string, not empty.';

}