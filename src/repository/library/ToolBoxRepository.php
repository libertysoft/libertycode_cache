<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\cache\repository\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\cache\repository\api\RepositoryInterface;



class ToolBoxRepository extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get specified item,
     * from specified repository, if found.
     * Get from specified item,
     * and set on repository after, else.
     *
     * Configuration array format:
     * @see RepositoryInterface::setItem() configuration array format.
     *
     * @param RepositoryInterface $objRepository
     * @param string $strKey
     * @param mixed|callable $item
     * @param array $tabConfig = null
     * @return mixed
     */
    public static function getSetItem(
        RepositoryInterface $objRepository,
        $strKey,
        $item,
        array $tabConfig = null
    )
    {
        // Init var
        $result = $objRepository->getItem($strKey, $item);

        // Add item, if required
        static::addItem($objRepository, $strKey, $result, $tabConfig);

        // Return result
        return $result;
    }



    /**
     * Get array of items,
     * from specified repository, if found.
     * Get from specified items,
     * and set on repository after, else.
     *
     * Associative array of items format:
     * @see RepositoryInterface::setTabItem() associative array of items format.
     *
     * Configuration array format:
     * @see RepositoryInterface::setTabItem() configuration array format.
     *
     * @param RepositoryInterface $objRepository
     * @param array $tabItem
     * @param array $tabConfig = null
     * @return array
     */
    public static function getTabSetItem(
        RepositoryInterface $objRepository,
        array $tabItem,
        array $tabConfig = null
    )
    {
        // Init var
        $result = $objRepository->getTabItem($tabItem);

        // Add items, if required
        static::addTabItem($objRepository, $tabItem, $tabConfig);

        // Return result
        return $result;
    }



    /**
     * Get specified item,
     * from specified repository, if found.
     * Remove after, if required.
     *
     * @param RepositoryInterface $objRepository
     * @param string $strKey
     * @param mixed|callable $default
     * @return mixed
     */
    public static function getRemoveItem(
        RepositoryInterface $objRepository,
        $strKey,
        $default
    )
    {
        // Init var
        $result = $objRepository->getItem($strKey, $default);

        // Remove item, if required
        $objRepository->removeItem($strKey);

        // Return result
        return $result;
    }



    /**
     * Get array of items,
     * from specified array of keys,
     * from specified repository, if found.
     * Remove after, if required.
     *
     * Array of keys format:
     * @see RepositoryInterface::getTabItem() array of keys format.
     *
     * @param RepositoryInterface $objRepository
     * @param array $tabKey
     * @return mixed
     */
    public static function getTabRemoveItem(
        RepositoryInterface $objRepository,
        array $tabKey
    )
    {
        // Init var
        $result = $objRepository->getTabItem($tabKey);

        // Remove items, if required
        $tabKey = array_keys($result);
        $objRepository->removeTabItem($tabKey);

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Add specified item.
     * Return true if success, false if an error occurs.
     *
     * Configuration array format:
     * @see RepositoryInterface::setItem() configuration array format.
     *
     * @param RepositoryInterface $objRepository
     * @param string $strKey
     * @param mixed|callable $item
     * @param array $tabConfig = null
     * @return boolean
     */
    public static function addItem(
        RepositoryInterface $objRepository,
        $strKey,
        $item,
        array $tabConfig = null
    )
    {
        // Return result
        return (
            (!$objRepository->checkItemExists($strKey)) ?
                $objRepository->setItem($strKey, $item, $tabConfig) :
                false
        );
    }



    /**
     * Add specified associative array of items.
     * Return true if all items success,
     * false if an error occurs on at least one item.
     *
     * Associative array of items format:
     * @see RepositoryInterface::setTabItem() associative array of items format.
     *
     * Configuration array format:
     * @see RepositoryInterface::setTabItem() configuration array format.
     *
     * @param RepositoryInterface $objRepository
     * @param array $tabItem
     * @param array $tabConfig = null
     * @return boolean
     */
    public static function addTabItem(
        RepositoryInterface $objRepository,
        array $tabItem,
        array $tabConfig = null
    )
    {
        // Init var
        $tabKey = array_keys($tabItem);
        $tabKeyFound = array();
        $tabKeyNotFound = array();
        $result = (
            // Add new items, if required
            (!$objRepository->checkTabItemExists(
                $tabKey,
                false,
                $tabKeyFound,
                $tabKeyNotFound
            )) ?
                (
                    // Add new items
                    $objRepository->setTabItem(
                        // Get new items
                        array_filter(
                            $tabItem,
                            function($strKey) use ($tabKeyNotFound) {return in_array($strKey, $tabKeyNotFound);},
                            ARRAY_FILTER_USE_KEY
                        ),
                        $tabConfig
                    ) &&
                    (count($tabItem) == count($tabKeyNotFound))
                ) :
                false
        );

        // Return result
        return $result;
    }



}