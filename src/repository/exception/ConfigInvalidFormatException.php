<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\cache\repository\exception;

use Exception;

use liberty_code\cache\repository\library\ConstRepository;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstRepository::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid key pattern
            (
                (!isset($config[ConstRepository::TAB_CONFIG_KEY_KEY_PATTERN])) ||
                (
                    is_string($config[ConstRepository::TAB_CONFIG_KEY_KEY_PATTERN]) &&
                    (trim($config[ConstRepository::TAB_CONFIG_KEY_KEY_PATTERN]) != '')
                )
            ) &&

            // Check valid key REGEXP pattern selection
            (
                (!isset($config[ConstRepository::TAB_CONFIG_KEY_KEY_REGEXP_SELECT])) ||
                (
                    is_string($config[ConstRepository::TAB_CONFIG_KEY_KEY_REGEXP_SELECT]) &&
                    (trim($config[ConstRepository::TAB_CONFIG_KEY_KEY_REGEXP_SELECT]) != '')
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}