<?php
/**
 * Description :
 * This class allows to define default repository class.
 * Can be consider is base of all repository type.
 *
 * Default repository uses the following specified configuration:
 * [
 *     key_pattern(optional):
 *         "string sprintf pattern,
 *         to build key used on repository, where '%1$s' replaced by specified key",
 *
 *     key_regexp_select(optional):
 *         "string REGEXP pattern, used on specified key used on repository,
 *         where first selection is considered as key"
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\cache\repository\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\cache\repository\api\RepositoryInterface;

use Closure;
use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\register\register\api\RegisterInterface;
use liberty_code\cache\repository\library\ConstRepository;
use liberty_code\cache\repository\exception\RegisterInvalidFormatException;
use liberty_code\cache\repository\exception\ConfigInvalidFormatException;
use liberty_code\cache\repository\exception\KeyInvalidFormatException;



/**
 * @method void setTabConfig(array $tabConfig) Set config array.
 */
class DefaultRepository extends FixBean implements RepositoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 * @param array $tabConfig = null
     * @param RegisterInterface $objRegister = null
     */
	public function __construct(
        array $tabConfig = null,
        RegisterInterface $objRegister = null
    )
	{
		// Call parent constructor
		parent::__construct();

        // Init configuration if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }

        // Init register if required
        if(!is_null($objRegister))
        {
            $this->setRegister($objRegister);
        }
	}
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstRepository::DATA_KEY_DEFAULT_REGISTER))
        {
            $this->__beanTabData[ConstRepository::DATA_KEY_DEFAULT_REGISTER] = null;
        }

        if(!$this->beanExists(ConstRepository::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstRepository::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstRepository::DATA_KEY_DEFAULT_REGISTER,
            ConstRepository::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRepository::DATA_KEY_DEFAULT_REGISTER:
                    RegisterInvalidFormatException::setCheck($value);
                    break;

                case ConstRepository::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkItemExists($strKey)
    {
        // Init var
        $objRegister = $this->getObjRegister();
        $result = (
            (!is_null($objRegister)) &&
            $objRegister->checkItemExists(
                $this->getRepoKeyFromKey($strKey, false)
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function checkTabItemExists(
        array $tabKey,
        $boolOneItemOnlyRequired = true,
        array &$tabKeyFound = array(),
        array &$tabKeyNotFound = array()
    )
    {
        // Init var
        $tabKey = array_values($tabKey);
        $boolOneItemOnlyRequired = (is_bool($boolOneItemOnlyRequired) ? $boolOneItemOnlyRequired : true);
        $tabKeyFound = array();
        $tabKeyNotFound = array();
        $result = (($boolOneItemOnlyRequired || (count($tabKey) == 0)) ? false : true);

        // Run each key
        foreach($tabKey as $strKey)
        {
            // Check item exists
            $boolItemExists = $this->checkItemExists($strKey);

            // Register key
            if($boolItemExists)
            {
                $tabKeyFound[] = $strKey;
            }
            else
            {
                $tabKeyNotFound[] = $strKey;
            }

            // Register check
            $result = (
                $boolOneItemOnlyRequired ?
                    ($result || $boolItemExists) :
                    ($result && $boolItemExists)
            );
        }

        // Return result
        return $result;

        // TODO register: bulk check items
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getObjRegister()
    {
        // Return result
        return $this->beanGet(ConstRepository::DATA_KEY_DEFAULT_REGISTER);
    }



    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Return result
        return $this->beanGet(ConstRepository::DATA_KEY_DEFAULT_CONFIG);
    }



    /**
     * Get string key(s) used on repository,
     * from specified key(s).
     *
     * @param string|string[] $key
     * @param boolean $boolArrayEnable = true
     * @return string|string[]
     * @throws KeyInvalidFormatException
     */
    protected function getRepoKeyFromKey($key, $boolArrayEnable = true)
    {
        // Init var
        $boolArrayEnable = (is_bool($boolArrayEnable) ? $boolArrayEnable : true);

        // Case array of keys
        if($boolArrayEnable && is_array($key))
        {
            // Init var
            $tabKey = array_values($key);
            $result = array_map(
                function($strKey) {return $this->getRepoKeyFromKey($strKey, false);},
                $tabKey
            );
        }
        // Case one key
        else
        {
            // Check arguments
            KeyInvalidFormatException::setCheck($key);

            // Init var
            $tabConfig = $this->getTabConfig();
            $strKey = $key;
            $result = (
                array_key_exists(ConstRepository::TAB_CONFIG_KEY_KEY_PATTERN, $tabConfig) ?
                    sprintf($tabConfig[ConstRepository::TAB_CONFIG_KEY_KEY_PATTERN], $strKey) :
                    $strKey
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get string key(s),
     * from specified key(s) used on repository.
     *
     * @param string|string[] $key
     * @param boolean $boolArrayEnable = true
     * @return string|string[]
     * @throws KeyInvalidFormatException
     */
    protected function getKeyFromRepoKey($key, $boolArrayEnable = true)
    {
        // Init var
        $boolArrayEnable = (is_bool($boolArrayEnable) ? $boolArrayEnable : true);

        // Case array of keys
        if($boolArrayEnable && is_array($key))
        {
            // Init var
            $tabKey = array_values($key);
            $result = array_map(
                function($strKey) {return $this->getKeyFromRepoKey($strKey, false);},
                $tabKey
            );
        }
        // Case one key
        else
        {
            // Check arguments
            KeyInvalidFormatException::setCheck($key);

            // Init var
            $tabConfig = $this->getTabConfig();
            $strKey = $key;
            $result = $strKey;

            // Select key, if required
            if(array_key_exists(ConstRepository::TAB_CONFIG_KEY_KEY_REGEXP_SELECT, $tabConfig))
            {
                $tabMatch = array();
                $strPattern = $tabConfig[ConstRepository::TAB_CONFIG_KEY_KEY_REGEXP_SELECT];
                $result = (
                (
                    (preg_match($strPattern, $result, $tabMatch) !== false) &&
                    isset($tabMatch[1])
                ) ?
                    $tabMatch[1] :
                    $result
                );
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get specified item value(s).
     *
     * @param mixed|callable|array $value
     * @param boolean $boolArrayEnable = true
     * @return mixed|array
     */
    protected function getItemValue($value, $boolArrayEnable = true)
    {
        // Init var
        $boolArrayEnable = (is_bool($boolArrayEnable) ? $boolArrayEnable : true);
        $result = (
            ($boolArrayEnable && is_array($value)) ?
                array_map(
                    function($value) {return $this->getItemValue($value, false);},
                    $value
                ) :
                (
                    (
                        is_callable($value) &&
                        ((!is_object($value)) || ($value instanceof Closure))
                    ) ?
                        $value() :
                        $value
                )
        );

        // Return result
        return $result;
    }



    /**
     * Get specified formatted item, when get action required.
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $item
     * @return mixed
     */
    protected function getItemFormatGet($strKey, $item)
    {
        // Format by attribute
        switch($strKey)
        {
            default:
                $result = $item;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * Get specified associative array of formatted items,
     * when get action required.
     *
     * Associative array of items format:
     * key => value: mixed item.
     *
     * @param array $tabItem
     * @return array
     */
    protected function getTabItemFormatGet(array $tabItem)
    {
        // Init var
        $result = array();

        // Run each item
        foreach($tabItem as $strKey => $item)
        {
            // Register formatted item
            $result[$strKey] = $this->getItemFormatGet($strKey, $item);
        }

        // Return result
        return $result;
    }



    /**
     * Get specified formatted item, when set action required.
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $item
     * @return mixed
     */
    protected function getItemFormatSet($strKey, $item)
    {
        // Format by attribute
        switch($strKey)
        {
            default:
                $result = $item;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * Get specified associative array of formatted items,
     * when set action required.
     *
     * Associative array of items format:
     * key => value: mixed item.
     *
     * @param array $tabItem
     * @return array
     */
    protected function getTabItemFormatSet(array $tabItem)
    {
        // Init var
        $result = array();

        // Run each item
        foreach($tabItem as $strKey => $item)
        {
            // Register formatted item
            $result[$strKey] = $this->getItemFormatSet($strKey, $item);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getItem($strKey, $default = null)
    {
        // Init var
        $objRegister = $this->getObjRegister();
        $result = (
            $this->checkItemExists($strKey) ?
                $this->getItemFormatGet(
                    $strKey,
                    $objRegister->getItem(
                        $this->getRepoKeyFromKey($strKey, false)
                    )
                ) :
                $this->getItemValue($default, false)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabItem(array $tabKey)
    {
        // Init var
        $result = array();

        // Case index array of keys: get only items found
        if(ToolBoxTable::checkTabIsIndex($tabKey))
        {
            $tabKeyFound = array();
            $result = (
                $this->checkTabItemExists($tabKey, true, $tabKeyFound) ?
                    array_combine(
                        $tabKeyFound,
                        array_map(
                            function($strKey) {return $this->getItem($strKey);},
                            $tabKeyFound
                        )
                    ) :
                    array()
            );
        }
        // Case associative array of keys: get all items (with default values, if required)
        else
        {
            foreach($tabKey as $strKey => $default)
            {
                $result[$strKey] = $this->getItem($strKey, $default);
            }
        }

        // Return result
        return $result;

        // TODO register: bulk get existing items
    }



    /**
     * @inheritdoc
     */
    public function getTabSearchKey(array $tabConfig = null)
    {
        // Init var
        $objRegister = $this->getObjRegister();
        $result = (
            (!is_null($objRegister)) ?
                $this->getKeyFromRepoKey(
                    $objRegister->getTabKey($tabConfig)
                ) :
                array()
        );

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setRegister(RegisterInterface $objRegister)
    {
        $this->beanSet(ConstRepository::DATA_KEY_DEFAULT_REGISTER, $objRegister);
    }



    /**
     * @inheritdoc
     */
    public function setItem($strKey, $item, array $tabConfig = null)
    {
        // Init var
        $objRegister = $this->getObjRegister();
        $strRepoKey = $this->getRepoKeyFromKey($strKey, false);
        $item = $this->getItemValue($item, false);
        $item = $this->getItemFormatSet($strKey, $item);
        $result = (
            (!is_null($objRegister)) ?
                (
                    $this->checkItemExists($strKey) ?
                        $objRegister->setItem($strRepoKey, $item, $tabConfig) :
                        $objRegister->addItem($strRepoKey, $item, $tabConfig)
                ) :
                false
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function setTabItem(array $tabItem, array $tabConfig = null)
    {
        // Init var
        $result = true;

        // Run each item
        foreach($tabItem as $strKey => $item)
        {
            // Set item
            $result =
                $this->setItem($strKey, $item, $tabConfig) &&
                $result;
        }

        // Return result
        return $result;

        // TODO register: bulk set existing items
        // TODO register: bulk add new items
    }



    /**
     * @inheritdoc
     */
    public function removeItem($strKey)
    {
        // Init var
        $objRegister = $this->getObjRegister();
        $result = (
            $this->checkItemExists($strKey) ?
                $objRegister->removeItem(
                    $this->getRepoKeyFromKey($strKey, false)
                ) :
                false
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeTabItem(array $tabKey)
    {
        // Init var
        $result = true;

        // Run each item
        foreach($tabKey as $strKey)
        {
            // Remove item
            $result =
                $this->removeItem($strKey) &&
                $result;
        }

        // Return result
        return $result;

        // TODO register: bulk remove existing items
        // (if existing items different from items provided, return false)
    }



    /**
     * @inheritdoc
     */
    public function removeItemAll()
    {
        // Init var
        $objRegister = $this->getObjRegister();
        $result = (
            (!is_null($objRegister)) &&
            $objRegister->removeItemAll()
        );

        // Return result
        return $result;
    }



}