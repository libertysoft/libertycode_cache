<?php
/**
 * Description :
 * This class allows to describe behavior of repository class.
 * Repository allows to design specific cache system,
 * containing all information to prepare item, to set in register.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\cache\repository\api;

use liberty_code\register\register\api\RegisterInterface;



interface RepositoryInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
	 * Check if specified item exists.
	 *
	 * @param string $strKey
	 * @return boolean
	 */
	public function checkItemExists($strKey);



    /**
     * Check if items exist,
     * from specified index array of keys.
     *
     * @param array $tabKey
     * @param boolean $boolOneItemOnlyRequired = true
     * @param array &$tabKeyFound = array()
     * @param array &$tabKeyNotFound = array()
     * @return boolean
     */
    public function checkTabItemExists(
        array $tabKey,
        $boolOneItemOnlyRequired = true,
        array &$tabKeyFound = array(),
        array &$tabKeyNotFound = array()
    );





    // Methods getters
    // ******************************************************************************

    /**
     * Get register object.
     *
     * @return null|RegisterInterface
     */
    public function getObjRegister();



    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig();



	/**
	 * Get specified item.
	 *
	 * @param string $strKey
     * @param mixed|callable $default = null
	 * @return null/mixed
	 */
	public function getItem($strKey, $default = null);



	/**
     * Get associative array of items,
     * from specified array of keys.
     * Return all keys provided if associative array of keys provided,
     * only keys found if index array of keys provided.
     *
     * Array of keys format:
     * Associative array of keys: key => value: null|mixed default
     * OR
     * Index array of keys.
     *
     * Return array format:
     * key => value: null|mixed item.
     *
     * @param array $tabKey
     * @return array
     */
    public function getTabItem(array $tabKey);



    /**
     * Get index array of searched keys,
     * from specified selection configuration.
     *
     * Configuration array format:
     * Selection configuration can be provided.
     * Null means no specific selection required and all keys will be selected.
     *
     * @param array $tabConfig = null
     * @return array
     */
    public function getTabSearchKey(array $tabConfig = null);





    // Methods setters
    // ******************************************************************************

    /**
     * Set register object.
     *
     * @param RegisterInterface $objRegister
     */
    public function setRegister(RegisterInterface $objRegister);



    /**
	 * Set specified item.
     * Return true if success, false if an error occurs.
	 *
     * Configuration array format:
     * Setting configuration can be provided.
     * Null means no specific option required during setting.
     *
	 * @param string $strKey
	 * @param mixed|callable $item
     * @param array $tabConfig = null
     * @return boolean
     */
	public function setItem($strKey, $item, array $tabConfig = null);



    /**
     * Set specified associative array of items.
     * Return true if all items success,
     * false if an error occurs on at least one item.
     *
     * Associative array of items format:
     * key => value: mixed|callable item.
     *
     * Configuration array format:
     * @see setItem() configuration array format.
     *
     * @param array $tabItem
     * @param array $tabConfig = null
     * @return boolean
     */
    public function setTabItem(array $tabItem, array $tabConfig = null);



	/**
	 * Remove specified item.
     * Return true if success, false if an error occurs.
	 *
	 * @param string $strKey
     * @return boolean: true if correctly removed, false else
     */
	public function removeItem($strKey);



    /**
     * Remove items,
     * from specified index array of keys.
     * Return true if all items success,
     * false if an error occurs on at least one item.
     *
     * @param array $tabKey
     * @return boolean
     */
    public function removeTabItem(array $tabKey);



    /**
     * Remove all items.
     * Return true if all items success,
     * false if an error occurs on at least one item.
     *
     * @return boolean
     */
    public function removeItemAll();
}