<?php
/**
 * Description :
 * This class allows to define format repository class.
 * Format repository is default repository, allowing to configure item formatting.
 *
 * Format repository uses the following specified configuration:
 * [
 *     Default repository configuration,
 *
 *     select_format_get_first_require(optional: got false if not found): true / false,
 *
 *     select_format_set_first_require(optional: got false if not found): true / false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\cache\repository\format\model;

use liberty_code\cache\repository\model\DefaultRepository;

use liberty_code\register\register\api\RegisterInterface;
use liberty_code\cache\format\model\FormatData;
use liberty_code\cache\repository\library\ConstRepository;
use liberty_code\cache\repository\format\library\ConstFormatRepository;
use liberty_code\cache\repository\format\exception\FormatDataInvalidFormatException;
use liberty_code\cache\repository\format\exception\ConfigInvalidFormatException;



/**
 * @method null|FormatData getObjFormatDataGet() Get format data object, used when get action required.
 * @method null|FormatData getObjFormatDataSet() Get format data object, used when set action required.
 * @method void setObjFormatDataGet(null|FormatData $objFormatDataGet) Set format data object, used when get action required.
 * @method void setObjFormatDataSet(null|FormatData $objFormatDataSet) Set format data object, used when set action required.
 */
class FormatRepository extends DefaultRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 * @param FormatData $objFormatDataGet = null
	 * @param FormatData $objFormatDataSet = null
     */
	public function __construct(
        array $tabConfig = null,
        RegisterInterface $objRegister = null,
	    FormatData $objFormatDataGet = null,
        FormatData $objFormatDataSet = null
    )
	{
		// Call parent constructor
		parent::__construct(
		    $tabConfig,
            $objRegister
        );
		
		// Hydrate format data get
		$this->setObjFormatDataGet($objFormatDataGet);
		
		// Hydrate format data set
        $this->setObjFormatDataSet($objFormatDataSet);
	}
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstFormatRepository::DATA_KEY_DEFAULT_FORMAT_DATA_GET))
        {
            $this->beanAdd(ConstFormatRepository::DATA_KEY_DEFAULT_FORMAT_DATA_GET, null);
        }

        if(!$this->beanExists(ConstFormatRepository::DATA_KEY_DEFAULT_FORMAT_DATA_SET))
        {
            $this->beanAdd(ConstFormatRepository::DATA_KEY_DEFAULT_FORMAT_DATA_SET, null);
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstFormatRepository::DATA_KEY_DEFAULT_FORMAT_DATA_GET,
            ConstFormatRepository::DATA_KEY_DEFAULT_FORMAT_DATA_SET
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstFormatRepository::DATA_KEY_DEFAULT_FORMAT_DATA_GET:
                case ConstFormatRepository::DATA_KEY_DEFAULT_FORMAT_DATA_SET:
                    FormatDataInvalidFormatException::setCheck($value);
                    break;

                case ConstRepository::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check select format callable first option required,
     * used when get action required,
     * for format callable search.
     *
     * @return boolean
     */
    public function checkSelectFormatGetFirstRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstFormatRepository::TAB_CONFIG_KEY_SELECT_FORMAT_GET_FIRST_REQUIRE]) &&
            (intval($tabConfig[ConstFormatRepository::TAB_CONFIG_KEY_SELECT_FORMAT_GET_FIRST_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check select format callable first option required,
     * used when set action required,
     * for format callable search.
     *
     * @return boolean
     */
    public function checkSelectFormatSetFirstRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstFormatRepository::TAB_CONFIG_KEY_SELECT_FORMAT_SET_FIRST_REQUIRE]) &&
            (intval($tabConfig[ConstFormatRepository::TAB_CONFIG_KEY_SELECT_FORMAT_SET_FIRST_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     *
     * Check if specified string format callable key matches,
     * from specified item.
     *
     * @param string $strFormatKey
     * @param string $strKey
     * @param mixed $item
     * @return boolean
     */
    protected function checkFormatKeyMatchesItem(
        $strFormatKey,
        $strKey,
        $item
    )
    {
        // Init var
        $strItemType = $this->getStrItemType($item);
        $strFormatKeyValue = $this->getStrFormatKeyCompareValue($strItemType, $strKey);
        $result = (
            is_string($strFormatKey) &&
            (preg_match($strFormatKey, $strFormatKeyValue) === 1)
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get string format callable key,
     * from specified string REGEXP type,
     * and specified string REGEXP key.
     *
     * @param null|string $strRegexpType = null
     * @param null|string $strRegexpKey = null
     * @return string
     */
    protected function getStrFormatKey(
        $strRegexpType = null,
        $strRegexpKey = null
    )
    {
        // Init var
        $strRegexpType = ((is_string($strRegexpType) && (trim($strRegexpType) != '')) ? $strRegexpType : '.*');
        $strRegexpKey = ((is_string($strRegexpKey) && (trim($strRegexpKey) != '')) ? $strRegexpKey : '.*');
        $result = sprintf(
            ConstFormatRepository::FORMAT_DATA_CONFIG_KEY_REGEXP_PATTERN,
            $strRegexpType,
            $strRegexpKey
        );

        // Return result
        return $result;
    }



    /**
     * Get string format callable key comparative value,
     * from specified string type,
     * and specified string key.
     *
     * @param null|string $strType = null
     * @param null|string $strKey = null
     * @return string
     */
    protected function getStrFormatKeyCompareValue(
        $strType = null,
        $strKey = null
    )
    {
        // Init var
        $strType = ((is_string($strType) && (trim($strType) != '')) ? $strType : '');
        $strKey = ((is_string($strKey) && (trim($strKey) != '')) ? $strKey : '');
        $result = sprintf(
            ConstFormatRepository::FORMAT_DATA_CONFIG_KEY_COMPARE_VALUE_PATTERN,
            $strType,
            $strKey
        );

        // Return result
        return $result;
    }



    /**
     * Get string item type,
     * from specified item.
     *
     * @param mixed $item
     * @return string
     */
    protected function getStrItemType($item)
    {
        // Init var
        $result = (
            is_object($item) ?
                get_class($item) :
                gettype($item)
        );

        // Return result
        return $result;
    }



    /**
     * Get format callable,
     * from specified format data object,
     * and specified item.
     *
     * @param FormatData $objFormatData
     * @param string $strKey
     * @param mixed $item
     * @param boolean $boolSelectFormatFirstRequired = false
     * @return null|callable
     */
    protected function getCallableFormat(
        FormatData $objFormatData,
        $strKey,
        $item,
        $boolSelectFormatFirstRequired = false
    )
    {
        // Init var
        $result = null;
        $tabFormatKey = array_keys($objFormatData->getDataSrc());
        $boolSelectFormatFirstRequired = (
            is_bool($boolSelectFormatFirstRequired) ?
                $boolSelectFormatFirstRequired :
                false
        );

        // Run each format callable
        for(
            $intCpt = 0;
            ($intCpt < count($tabFormatKey)) &&
            (
                (!$boolSelectFormatFirstRequired) ||
                is_null($result)
            );
            $intCpt++
        )
        {
            // Get info
            $strFormatKey = $tabFormatKey[$intCpt];

            // Register format callable, if required (item matches)
            $result = (
                $this->checkFormatKeyMatchesItem($strFormatKey, $strKey, $item) ?
                    $objFormatData->getValue($strFormatKey) :
                    $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getItemFormatGet($strKey, $item)
    {
        // Init var
        $objFormatData = $this->getObjFormatDataGet();
        $callableFormat = (
            (!is_null($objFormatData)) ?
                $this->getCallableFormat(
                    $objFormatData,
                    $strKey,
                    $item,
                    $this->checkSelectFormatGetFirstRequired()
                ) :
                null
        );
        $result = (
            (!is_null($callableFormat)) ?
                $callableFormat($strKey, $item) :
                parent::getItemFormatGet($strKey, $item)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getItemFormatSet($strKey, $item)
    {
        // Init var
        $objFormatData = $this->getObjFormatDataSet();
        $callableFormat = (
            (!is_null($objFormatData)) ?
                $this->getCallableFormat(
                    $objFormatData,
                    $strKey,
                    $item,
                    $this->checkSelectFormatSetFirstRequired()
                ) :
                null
        );
        $result = (
            (!is_null($callableFormat)) ?
                $callableFormat($strKey, $item) :
                parent::getItemFormatSet($strKey, $item)
        );

        // Return result
        return $result;
    }





    // Methods format
    // ******************************************************************************

    /**
     * Check if format callable, used when get action required, exists,
     * from specified string REGEXP type,
     * and specified string REGEXP key.
     *
     * @param null|string $strRegexpType = null
     * @param null|string $strRegexpKey = null
     * @return boolean
     */
    public function checkFormatGetExists(
        $strRegexpType = null,
        $strRegexpKey = null
    )
    {
        // Init var
        $objFormatData = $this->getObjFormatDataGet();
        $strFormatKey = $this->getStrFormatKey(
            $strRegexpType,
            $strRegexpKey
        );
        $result = (
            (!is_null($objFormatData)) ?
                $objFormatData->checkValueExists($strFormatKey) :
                false
        );

        // Return result
        return $result;
    }



    /**
     * Check if format callable, used when set action required, exists,
     * from specified string REGEXP type,
     * and specified string REGEXP key.
     *
     * @param null|string $strRegexpType = null
     * @param null|string $strRegexpKey = null
     * @return boolean
     */
    public function checkFormatSetExists(
        $strRegexpType = null,
        $strRegexpKey = null
    )
    {
        // Init var
        $objFormatData = $this->getObjFormatDataSet();
        $strFormatKey = $this->getStrFormatKey(
            $strRegexpType,
            $strRegexpKey
        );
        $result = (
            (!is_null($objFormatData)) ?
                $objFormatData->checkValueExists($strFormatKey) :
                false
        );

        // Return result
        return $result;
    }



    /**
     * Get format callable, used when get action required,
     * from specified string REGEXP type,
     * and specified string REGEXP key.
     *
     * @param null|string $strRegexpType = null
     * @param null|string $strRegexpKey = null
     * @return null|callable
     */
    public function getCallableFormatGet(
        $strRegexpType = null,
        $strRegexpKey = null
    )
    {
        // Init var
        $objFormatData = $this->getObjFormatDataGet();
        $strFormatKey = $this->getStrFormatKey(
            $strRegexpType,
            $strRegexpKey
        );
        $result = (
            (!is_null($objFormatData)) ?
                $objFormatData->getValue($strFormatKey) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get format callable, used when set action required,
     * from specified string REGEXP type,
     * and specified string REGEXP key.
     *
     * @param null|string $strRegexpType = null
     * @param null|string $strRegexpKey = null
     * @return null|callable
     */
    public function getCallableFormatSet(
        $strRegexpType = null,
        $strRegexpKey = null
    )
    {
        // Init var
        $objFormatData = $this->getObjFormatDataSet();
        $strFormatKey = $this->getStrFormatKey(
            $strRegexpType,
            $strRegexpKey
        );
        $result = (
            (!is_null($objFormatData)) ?
                $objFormatData->getValue($strFormatKey) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Set (update if exists, add else) specified format callable, used when get action required,
     * for specified string REGEXP type,
     * and specified string REGEXP key.
     *
     * Callable format:
     * mixed function(string $strKey, mixed $item):
     * Allows to get mixed formatted item to provide, from mixed stored item.
     *
     * @param callable $callable
     * @param null|string $strRegexpType = null
     * @param null|string $strRegexpKey = null
     * @return boolean
     */
    public function setFormatGet(
        $callable,
        $strRegexpType = null,
        $strRegexpKey = null
    )
    {
        // Init var
        $objFormatData = $this->getObjFormatDataGet();
        $strFormatKey = $this->getStrFormatKey(
            $strRegexpType,
            $strRegexpKey
        );
        $result = (
            (!is_null($objFormatData)) ?
                $objFormatData->putValue($strFormatKey, $callable) :
                false
        );

        // Return result
        return $result;
    }



    /**
     * Set (update if exists, add else) specified format callable, used when set action required,
     * for specified string REGEXP type,
     * and specified string REGEXP key.
     *
     * Callable format:
     * mixed function(string $strKey, mixed $item):
     * Allows to get mixed formatted item to store, from mixed provided item.
     *
     * @param callable $callable
     * @param null|string $strRegexpType = null
     * @param null|string $strRegexpKey = null
     * @return boolean
     */
    public function setFormatSet(
        $callable,
        $strRegexpType = null,
        $strRegexpKey = null
    )
    {
        // Init var
        $objFormatData = $this->getObjFormatDataSet();
        $strFormatKey = $this->getStrFormatKey(
            $strRegexpType,
            $strRegexpKey
        );
        $result = (
            (!is_null($objFormatData)) ?
                $objFormatData->putValue($strFormatKey, $callable) :
                false
        );

        // Return result
        return $result;
    }



    /**
     * Remove specified format callable, used when get action required,
     * from specified string REGEXP type,
     * and specified string REGEXP key.
     *
     * @param null|string $strRegexpType = null
     * @param null|string $strRegexpKey = null
     * @return boolean
     */
    public function removeFormatGet(
        $strRegexpType = null,
        $strRegexpKey = null
    )
    {
        // Init var
        $objFormatData = $this->getObjFormatDataGet();
        $strFormatKey = $this->getStrFormatKey(
            $strRegexpType,
            $strRegexpKey
        );
        $result = (
            (!is_null($objFormatData)) ?
                $objFormatData->removeValue($strFormatKey) :
                false
        );

        // Return result
        return $result;
    }



    /**
     * Remove specified format callable, used when set action required,
     * from specified string REGEXP type,
     * and specified string REGEXP key.
     *
     * @param null|string $strRegexpType = null
     * @param null|string $strRegexpKey = null
     * @return boolean
     */
    public function removeFormatSet(
        $strRegexpType = null,
        $strRegexpKey = null
    )
    {
        // Init var
        $objFormatData = $this->getObjFormatDataSet();
        $strFormatKey = $this->getStrFormatKey(
            $strRegexpType,
            $strRegexpKey
        );
        $result = (
            (!is_null($objFormatData)) ?
                $objFormatData->removeValue($strFormatKey) :
                false
        );

        // Return result
        return $result;
    }



}