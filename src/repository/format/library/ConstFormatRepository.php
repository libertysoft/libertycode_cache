<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\cache\repository\format\library;



class ConstFormatRepository
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_FORMAT_DATA_GET = 'objFormatDataGet';
    const DATA_KEY_DEFAULT_FORMAT_DATA_SET = 'objFormatDataSet';



    // Format data configuration
    const FORMAT_DATA_CONFIG_KEY_REGEXP_PATTERN = '#^type\:%1$s\;key\:%2$s$#';
    const FORMAT_DATA_CONFIG_KEY_COMPARE_VALUE_PATTERN = 'type:%1$s;key:%2$s';

    // Configuration
    const TAB_CONFIG_KEY_SELECT_FORMAT_GET_FIRST_REQUIRE = 'select_format_get_first_require';
    const TAB_CONFIG_KEY_SELECT_FORMAT_SET_FIRST_REQUIRE = 'select_format_set_first_require';



    // Exception message constants
    const EXCEPT_MSG_FORMAT_DATA_INVALID_FORMAT = 'Following format data "%1$s" invalid! It must be null or a format data object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! The config must be an array, not empty and following the format repository configuration standard.';



}