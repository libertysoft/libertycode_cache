<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\cache\repository\format\build\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\cache\repository\format\model\FormatRepository;



class ToolBoxFormatBuilder extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate format callable,
     * using serialization features,
     * on specified format repository object,
     * for specified string REGEXP type,
     * and specified string REGEXP key.
     *
     * @param FormatRepository $objFormatRepo
     * @param null|string $strRegexpType = null
     * @param null|string $strRegexpKey = null
     * @param boolean $boolFormatGetRequired = true
     * @param boolean $boolFormatSetRequired = true
     */
    public static function hydrateSerialFormat(
        FormatRepository $objFormatRepo,
        $strRegexpType = null,
        $strRegexpKey = null,
        $boolFormatGetRequired = true,
        $boolFormatSetRequired = true
    )
    {
        // Init var
        $boolFormatGetRequired = (is_bool($boolFormatGetRequired) ? $boolFormatGetRequired : true);
        $boolFormatSetRequired = (is_bool($boolFormatSetRequired) ? $boolFormatSetRequired : true);

        // Set format callable, used when get action required, if required
        if($boolFormatGetRequired)
        {
            $objFormatRepo->setFormatGet(
                array(static::class, 'getItemSerialFormatGet'),
                $strRegexpType,
                $strRegexpKey
            );
        }

        // Set format callable, used when set action required, if required
        if($boolFormatSetRequired)
        {
            $objFormatRepo->setFormatSet(
                array(static::class, 'getItemSerialFormatSet'),
                $strRegexpType,
                $strRegexpKey
            );
        }
    }



    /**
     * Hydrate format callable,
     * using JSON features,
     * on specified format repository object,
     * for specified string REGEXP type,
     * and specified string REGEXP key.
     *
     * @param FormatRepository $objFormatRepo
     * @param null|string $strRegexpType = null
     * @param null|string $strRegexpKey = null
     * @param boolean $boolFormatGetRequired = true
     * @param boolean $boolFormatSetRequired = true
     */
    public static function hydrateJsonFormat(
        FormatRepository $objFormatRepo,
        $strRegexpType = null,
        $strRegexpKey = null,
        $boolFormatGetRequired = true,
        $boolFormatSetRequired = true
    )
    {
        // Init var
        $boolFormatGetRequired = (is_bool($boolFormatGetRequired) ? $boolFormatGetRequired : true);
        $boolFormatSetRequired = (is_bool($boolFormatSetRequired) ? $boolFormatSetRequired : true);

        // Set format callable, used when get action required, if required
        if($boolFormatGetRequired)
        {
            $objFormatRepo->setFormatGet(
                array(static::class, 'getItemJsonFormatGet'),
                $strRegexpType,
                $strRegexpKey
            );
        }

        // Set format callable, used when set action required, if required
        if($boolFormatSetRequired)
        {
            $objFormatRepo->setFormatSet(
                array(static::class, 'getItemJsonFormatSet'),
                $strRegexpType,
                $strRegexpKey
            );
        }
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get specified formatted item,
     * using serialization features,
     * when get action required.
     *
     * @param string $strKey
     * @param mixed $item
     * @return mixed
     */
    public static function getItemSerialFormatGet($strKey, $item)
    {
        // Format by attribute
        switch($strKey)
        {
            default:
                $result = unserialize($item);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * Get specified formatted item,
     * using JSON features,
     * when get action required.
     *
     * @param string $strKey
     * @param mixed $item
     * @return mixed
     */
    public static function getItemJsonFormatGet($strKey, $item)
    {
        // Format by attribute
        switch($strKey)
        {
            default:
                $result = json_decode($item, true);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * Get specified formatted item,
     * using serialization features,
     * when set action required.
     *
     * @param string $strKey
     * @param mixed $item
     * @return mixed
     */
    public static function getItemSerialFormatSet($strKey, $item)
    {
        // Format by attribute
        switch($strKey)
        {
            default:
                $result = serialize($item);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * Get specified formatted item,
     * using JSON features,
     * when set action required.
     *
     * @param string $strKey
     * @param mixed $item
     * @return mixed
     */
    public static function getItemJsonFormatSet($strKey, $item)
    {
        // Format by attribute
        switch($strKey)
        {
            default:
                $result = json_encode($item);
                break;
        }

        // Return result
        return $result;
    }



}