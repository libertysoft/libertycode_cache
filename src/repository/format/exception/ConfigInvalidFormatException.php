<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\cache\repository\format\exception;

use Exception;

use liberty_code\cache\repository\format\library\ConstFormatRepository;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstFormatRepository::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid select format callable get first required option
            (
                (!isset($config[ConstFormatRepository::TAB_CONFIG_KEY_SELECT_FORMAT_GET_FIRST_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstFormatRepository::TAB_CONFIG_KEY_SELECT_FORMAT_GET_FIRST_REQUIRE]) ||
                    is_int($config[ConstFormatRepository::TAB_CONFIG_KEY_SELECT_FORMAT_GET_FIRST_REQUIRE]) ||
                    (
                        is_string($config[ConstFormatRepository::TAB_CONFIG_KEY_SELECT_FORMAT_GET_FIRST_REQUIRE]) &&
                        ctype_digit($config[ConstFormatRepository::TAB_CONFIG_KEY_SELECT_FORMAT_GET_FIRST_REQUIRE])
                    )
                )
            ) &&

            // Check valid select format callable set first required option
            (
                (!isset($config[ConstFormatRepository::TAB_CONFIG_KEY_SELECT_FORMAT_SET_FIRST_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstFormatRepository::TAB_CONFIG_KEY_SELECT_FORMAT_SET_FIRST_REQUIRE]) ||
                    is_int($config[ConstFormatRepository::TAB_CONFIG_KEY_SELECT_FORMAT_SET_FIRST_REQUIRE]) ||
                    (
                        is_string($config[ConstFormatRepository::TAB_CONFIG_KEY_SELECT_FORMAT_SET_FIRST_REQUIRE]) &&
                        ctype_digit($config[ConstFormatRepository::TAB_CONFIG_KEY_SELECT_FORMAT_SET_FIRST_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}