<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\cache\repository\format\exception;

use Exception;

use liberty_code\cache\format\model\FormatData;
use liberty_code\cache\repository\format\library\ConstFormatRepository;



class FormatDataInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $formatData
     */
	public function __construct($formatData)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstFormatRepository::EXCEPT_MSG_FORMAT_DATA_INVALID_FORMAT,
            mb_strimwidth(strval($formatData), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified format data has valid format.
	 * 
     * @param mixed $formatData
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($formatData)
    {
		// Init var
		$result = (
		    is_null($formatData) ||
            ($formatData instanceof FormatData)
        );

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($formatData);
		}
		
		// Return result
		return $result;
    }
	
	
	
}