<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\cache\format\model\FormatData;
use liberty_code\cache\repository\format\model\FormatRepository;
use liberty_code\cache\repository\format\build\library\ToolBoxFormatBuilder;



// Init register
$objRegister = new DefaultTableRegister();

// Init format data
$objFormatDataGet = new FormatData();
$objFormatDataSet = new FormatData();

// Init format repository
$objFormatRepo = new FormatRepository(
    null,
    $objRegister,
    $objFormatDataGet,
    $objFormatDataSet
);



// Hydrate format callable
ToolBoxFormatBuilder::hydrateSerialFormat($objFormatRepo);


