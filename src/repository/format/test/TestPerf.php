<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/repository/format/test/RepositoryTest.php');

// Use
use liberty_code\cache\repository\library\ToolBoxRepository;



//*
// Hydrate format callable
$mtStart = microtime(true);

for($intCpt = 0; $intCpt < 50; $intCpt++)
{
    $strRegexpKey = sprintf('key\-%1$d', $intCpt);
    $objFormatRepo->setFormatGet(
        function($strKey, $item) {
            return $item;
        },
        null,
        $strRegexpKey
    );
    $objFormatRepo->setFormatSet(
        function($strKey, $item) {
            return $item;
        },
        null,
        $strRegexpKey
    );
}

$mtEnd = microtime(true);
var_dump(sprintf('Time hydration format: %1$s', strval($mtEnd - $mtStart)));
//*/



//*
// Hydrate items
$mtStart = microtime(true);

$tabItem = array();
for($intCpt = 0; $intCpt < 1000; $intCpt++)
{
    // Set item
    $strKey = sprintf('key-%1$d', $intCpt);
    $value = sprintf('Value %1$d', $intCpt);
    $tabItem[$strKey] = $value;
}

$objFormatRepo->setTabItem($tabItem);

$mtEnd = microtime(true);
var_dump(sprintf('Time hydration items: %1$s', strval($mtEnd - $mtStart)));
//*/



/*
// Get items
$mtStart = microtime(true);

$tabKey = $objFormatRepo->getTabSearchKey();
foreach($tabKey as $strKey)
{
    $value = $objFormatRepo->getItem($strKey);
}

$mtEnd = microtime(true);
var_dump(sprintf('Time get items: %1$s', strval($mtEnd - $mtStart)));
//*/



/*
// Get specific items
$mtStart = microtime(true);

for($intCpt = 0; $intCpt < 100; $intCpt++)
{
    $strKey = sprintf('key-%1$d', $intCpt);
    $value = $objFormatRepo->getItem($strKey);
}

$mtEnd = microtime(true);
var_dump(sprintf('Time get specific items: %1$s', strval($mtEnd - $mtStart)));
//*/



//*
// Get/hydrate items
$mtStart = microtime(true);

$tabItem = array();
for($intCpt = 0; $intCpt < 100; $intCpt++)
{
    // Set item
    $strKey = sprintf('key-bis-%1$d', $intCpt);
    $value = sprintf('Value %1$d', $intCpt);
    $tabItem[$strKey] = $value;
}

$tabValue = ToolBoxRepository::getTabSetItem($objFormatRepo, $tabItem);

$mtEnd = microtime(true);
var_dump(sprintf('Time get/hydration items: %1$s', strval($mtEnd - $mtStart)));


$mtStart = microtime(true);

$tabValue = ToolBoxRepository::getTabSetItem($objFormatRepo, $tabItem);

$mtEnd = microtime(true);
var_dump(sprintf('Time get/hydration items (twice): %1$s', strval($mtEnd - $mtStart)));


echo('Test get/hydration items: <br />');
echo('<pre>');var_dump($tabValue);echo('</pre>');

echo('<br /><br /><br />');
//*/


