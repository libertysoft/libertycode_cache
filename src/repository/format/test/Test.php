<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/repository/format/test/RepositoryTest.php');



// Test register property
echo('Test register property : <br />');

echo('Test register: <pre>');print_r($objFormatRepo->getObjRegister());echo('</pre>');

echo('<br /><br /><br />');



// Test config property
echo('Test config property : <br />');

try{
    $objFormatRepo->setTabConfig('test');
} catch(\Exception $e) {
    echo($e->getMessage());
    echo('<br />');
}

$objFormatRepo->setTabConfig(array(
    'key_pattern' => 'cache-%1$s',
    'key_regexp_select' => '#cache\-(.*)#'
));
echo('Test config: <pre>');print_r($objFormatRepo->getTabConfig());echo('</pre>');

echo('<br /><br /><br />');



// Test format
echo('Test format get: <br />');
echo('<pre>');print_r($objFormatRepo->getObjFormatDataGet()->getDataSrc());echo('</pre>');
echo('<br />');

echo('Test format set: <br />');
echo('<pre>');print_r($objFormatRepo->getObjFormatDataSet()->getDataSrc());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');



// Test item set
$tabItem = array(
    'config.Load' => 'Value test', // Ok

    // Ok
    'config.Di' => array(
        'key_test_get_1' => 1,
        'key_test_get_2' => true,
        'key_test_get_N' => 'Value test N',
    ),

    'config/Di' => 'Value test Di', // Ok
    'config.Load_2' => 'Value load test 2', // Ok
    'config.Di_2' => -7.75, // Ok
    'config.Di_3' => 'Value Di test 3' // Ok
);

foreach($tabItem as $strKey => $item)
{
    echo('Test set item "'.$strKey.'": <br />');
    try{
        echo('<pre>');var_dump($objFormatRepo->setItem($strKey, $item));echo('</pre>');
    } catch(\Exception $e) {
        echo(htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('Test setting item: <br />');
echo('<pre>');print_r($objFormatRepo->getTabItem($objFormatRepo->getTabSearchKey()));echo('</pre>');
echo('Test setting item: register:  <br />');
echo('<pre>');print_r($objFormatRepo->getObjRegister()->getTabItem());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');



// Test item check, get
$tabKey = array(
    'config.Load', // Found
    'config/Load/Set', // Not found
    'config.Di', // Found
    'config.Di_2', // Found
    'test' // Not found
);

foreach($tabKey as $strKey)
{
    echo('Test check, get item "'.$strKey.'": <br />');
    try{
        if($objFormatRepo->checkItemExists($strKey))
        {
            echo('<pre>');var_dump($objFormatRepo->getItem($strKey));echo('</pre>');
        }
        else
        {
            echo('Key not found<br />');
        }
    } catch(\Exception $e) {
        echo(htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('<br /><br /><br />');


echo('Test check array of items (once): <br />');
$tabKeyFound = array();
$tabKeyNotFound = array();
echo('Check: ');
var_dump($objFormatRepo->checkTabItemExists(
    $tabKey,
    true,
    $tabKeyFound,
    $tabKeyNotFound
));
echo('Keys found: <pre>');print_r($tabKeyFound);echo('</pre>');
echo('Keys not found: <pre>');print_r($tabKeyNotFound);echo('</pre>');

echo('<br /><br /><br />');


echo('Test check array of items (all): <br />');
echo('Check: ');
var_dump($objFormatRepo->checkTabItemExists(
    $tabKey,
    false,
    $tabKeyFound,
    $tabKeyNotFound
));
echo('Keys found: <pre>');print_r($tabKeyFound);echo('</pre>');
echo('Keys not found: <pre>');print_r($tabKeyNotFound);echo('</pre>');

echo('<br /><br /><br />');



// Test item remove
$tabKey = array(
    'config.Load', // Found
    'config/Load/Set', // Not found
    'config.Di', // Found
    'test' // Not found
);

foreach($tabKey as $strKey)
{
    echo('Test remove item "'.$strKey.'": <br />');
    try{
        echo('<pre>');var_dump($objFormatRepo->removeItem($strKey));echo('</pre>');
    } catch(\Exception $e) {
        echo(htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('Test removing item: <br />');
echo('<pre>');print_r($objFormatRepo->getTabItem($objFormatRepo->getTabSearchKey()));echo('</pre>');
echo('Test removing item: register:  <br />');
echo('<pre>');print_r($objFormatRepo->getObjRegister()->getTabItem());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');



// Test all items remove
//$objFormatRepo->removeTabItem($objFormatRepo->getTabSearchKey());
$objFormatRepo->removeItemAll();

echo('Test remove all items: <br />');
echo('<pre>');print_r($objFormatRepo->getTabSearchKey());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');



// Test array of items set
echo('Test set array of items: <br />');
echo('<pre>');var_dump($objFormatRepo->setTabItem($tabItem));echo('</pre>');

echo('Test setting array of items: <br />');
echo('<pre>');print_r($objFormatRepo->getTabItem($objFormatRepo->getTabSearchKey()));echo('</pre>');
echo('Test setting array of items: register:  <br />');
echo('<pre>');print_r($objFormatRepo->getObjRegister()->getTabItem());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');


